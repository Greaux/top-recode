@echo off

echo Compiling top-recode game sources
echo ----------------------------------------
echo (pass --help as a parameter to see help)
echo 

goto :set_param_vars

:help
echo Usage: compile_src.bat [options]
echo - Compiles/Updates all code, language files and bin files,
echo and creates all symlinks for this project.
echo - Requires admin rights only for first run (for symlink
echo creation).
echo 
echo Options:
echo 
echo --help         - Shows this help screen.
echo --debug        - Compiles the debug of projects instead
echo                Note: Luajit debug files will replace
echo                release files!
echo --default_mod  - Uses the "default" lua server scripts
echo                (they are in alpha at the moment).
echo                Requires SQL Server to be running!
echo --pull         - Git pulls project before starting.
echo                Won't work if there are modified files.
echo --mssql2000    - Will copy SQL 2000 dbs instead of 2008's.
goto :exit

rem From https://stackoverflow.com/questions/3973824/windows-bat-file-optional-argument-parsing

:set_param_vars
set DEBUG=
set BUILD_METHOD=Release
set DEFAULT_MOD=n
set GIT_PULL=n
set MSSQL2000=n

:params_parse
if not "%1"=="" (
	if "%1"=="--debug" (
		set DEBUG=debug
		set BUILD_METHOD=Debug
	)
	if "%1"=="--default_mod" set DEFAULT_MOD=y
	if "%1"=="--help" goto :help
	if "%1"=="--pull" set GIT_PULL=y
	if "%1"=="--mssql2000" set MSSQL2000=y
	
	shift
	goto :params_parse
)

rem Check if this bat file is going to be self-modified by a git pull
if %GIT_PULL%==y (
	echo Git pulling to make sure you're using latest files

	call git diff --exit-code %~f0
	if %ERRORLEVEL% NEQ 0 (
		echo This script gets modified on git pull. Git pulling on continue.
		echo Please, restart this script once it finishes.
		pause
		rem call git pull
		goto :exit
	) else (
		rem call git pull
	)
)

rem From https://stackoverflow.com/questions/4051883/batch-script-how-to-check-for-admin-rights
set ADMIN=n
net session >nul 2>&1
if %errorLevel% == 0 (
    set ADMIN=y
)

set FILE=
set SOURCE_FILE=

echo Loading Microsoft Visual Studio 2019 variables

:: Script to detect proper VS folder
if not defined PROGRAMFILES(X86) (
	set MYPROGRAMFILES=%PROGRAMFILES%
) else set MYPROGRAMFILES=%PROGRAMFILES(X86)%
echo %MYPROGRAMFILES%
if exist "%MYPROGRAMFILES%\Microsoft Visual Studio\2019\Community" (
	set MYEDITION=Community
) else (
	if exist "%MYPROGRAMFILES%\Microsoft Visual Studio\2019\Professional" (
		set MYEDITION=Professional
	) else (
		if exist "%MYPROGRAMFILES%\Microsoft Visual Studio\2019\Enterprise" set MYEDITION=Enterprise
	)
)

rem if block fixes running vcversall many times in a row in same cmd prompt window.
if not defined DevEnvDir (
    call "%MYPROGRAMFILES%\Microsoft Visual Studio\2019\%MYEDITION%\VC\Auxiliary\Build\vcvarsall.bat" x86
)

:: (fix for Windows 8+) Change directory to the one script is on
cd /d %~dp0 

echo Cloning luajit
cd source\src
call git clone http://luajit.org/git/luajit-2.0.git luajit
cd ..\..
copy compatlj52-msvcbuild.bat source\src\luajit\src
cd source\src\luajit
if not exist "src\" (
	call git checkout v2.1
) else (
	call git pull
)

echo Compiling luajit
cd src
call compatlj52-msvcbuild.bat %DEBUG%
cd ..\..\..

echo Make symbolic links for luajit binaries for project building
cd lib
mkdir luajit
cd luajit
call :MKLINK "..\..\src\luajit\src\lua51.dll"
call :MKLINK "..\..\src\luajit\src\lua51.lib"
call :MKLINK "..\..\src\luajit\src\luajit.lib"
cd ..\..

cd inc
mkdir luajit
cd luajit
call :MKLINK "..\..\src\luajit\src\lauxlib.h"
call :MKLINK "..\..\src\luajit\src\lua.h"
call :MKLINK "..\..\src\luajit\src\lua.hpp"
call :MKLINK "..\..\src\luajit\src\luaconf.h"
call :MKLINK "..\..\src\luajit\src\luajit.h"
call :MKLINK "..\..\src\luajit\src\lualib.h"
cd ..\..\..

echo Compiling all projects belonging to project (this will take a long time!)
devenv /build "%BUILD_METHOD%" ".\source\src\all.sln"
devenv /build "%BUILD_METHOD% Client" ".\source\src\all.sln"

echo Make symbolic links for all the binaries created
cd client\system
call :MKLINK "..\..\source\lib\luajit\lua51.dll"
call :MKLINK "..\..\source\lib\SDL\SDL2.dll"
call :MKLINK "..\..\source\lib\SDL_mixer\SDL2_mixer.dll"
call :MKLINK "..\..\source\lib\SDL_mixer\libFLAC-8.dll"
call :MKLINK "..\..\source\lib\SDL_mixer\libmodplug-1.dll"
call :MKLINK "..\..\source\lib\SDL_mixer\libmpg123-0.dll"
call :MKLINK "..\..\source\lib\SDL_mixer\libogg-0.dll"
call :MKLINK "..\..\source\lib\SDL_mixer\libopus-0.dll"
call :MKLINK "..\..\source\lib\SDL_mixer\libopusfile-0.dll"
call :MKLINK "..\..\source\lib\SDL_mixer\libvorbis-0.dll"
call :MKLINK "..\..\source\lib\SDL_mixer\libvorbisfile-3.dll"
call :MKLINK "..\..\source\lib\ipss\PAI.dll"
if %BUILD_METHOD%==Debug (
	call :MKLINK "..\..\source\lib\MindPower3D\MindPower3DD_DX8.dll"
	call :MKLINK "..\..\source\lib\MindPower3D\MindPower3DD_DX8.pdb"
) else (
	call :MKLINK "..\..\source\lib\MindPower3D\MindPower3D_DX8.dll"
	call :MKLINK "..\..\source\lib\MindPower3D\MindPower3D_DX8.pdb"
)
cd ..\..

cd server
call :MKLINK "..\source\lib\luajit\lua51.dll"
cd ..

cd helpers
call :MKLINK "..\..\src\luajit\src\luajit.exe"
call :MKLINK "..\..\src\luajit\src\lua51.dll"
cd ..

echo Make symbolic links for icu63
cd translation
call :MKLINK "..\source\lib\icu\icudt63.dll"
call :MKLINK "..\source\lib\icu\icuin63.dll"
call :MKLINK "..\source\lib\icu\icuio63.dll"
call :MKLINK "..\source\lib\icu\icutu63.dll"
call :MKLINK "..\source\lib\icu\icuuc63.dll"
cd ..

echo Compiling translation resources...
cd translation
call compile.bat
cd ..

echo Symbolic/hard linking more files...
cd server
call :MKLINK "..\translation\en_US.res"
call :MKLINK "..\translation\pt_BR.res"
if %BUILD_METHOD%==Debug (
	call :MKLINK "..\source\lib\icu\icudt63d.dll"
	call :MKLINK "..\source\lib\icu\icuin63d.dll"
	call :MKLINK "..\source\lib\icu\icuuc63d.dll"
) else (
	call :MKLINK "..\source\lib\icu\icudt63.dll"
	call :MKLINK "..\source\lib\icu\icuin63.dll"
	call :MKLINK "..\source\lib\icu\icuuc63.dll"
)
rem Remember that derb.exe and genrb.exe are precompiled and don't use debug dlls
call :MKLINK_DIFFNAME  "AccountServer.loc" "..\translation\Game.loc"
call :MKLINK_DIFFNAME  "GameServer.loc" "..\translation\Game.loc"
call :MKLINK_DIFFNAME  "GateServer.loc" "..\translation\Game.loc"
call :MKLINK_DIFFNAME "GroupServer.loc" "..\translation\Game.loc"
cd ..\client\system
call :MKLINK "..\..\translation\en_US.res"
call :MKLINK "..\..\translation\pt_BR.res"
if %BUILD_METHOD%==Debug (
	call :MKLINK "..\..\source\lib\icu\icudt63d.dll"
	call :MKLINK "..\..\source\lib\icu\icuin63d.dll"
	call :MKLINK "..\..\source\lib\icu\icutu63d.dll"
	call :MKLINK "..\..\source\lib\icu\icuuc63d.dll"
) else (
	call :MKLINK "..\..\source\lib\icu\icudt63.dll"
	call :MKLINK "..\..\source\lib\icu\icuin63.dll"
	call :MKLINK "..\..\source\lib\icu\icutu63.dll"
	call :MKLINK "..\..\source\lib\icu\icuuc63.dll"
)
call :MKLINK "..\..\translation\Game.loc"
cd ..\scripts
call :MKLINK_DIFFNAME_J "table" "table-top2"
cd ..\..\resource
call :MKLINK "..\client\scripts\table\areaset.txt"
call :MKLINK "..\client\scripts\table\characterinfo.txt"
call :MKLINK "..\client\scripts\table\forgeitem.txt"
call :MKLINK "..\client\scripts\table\iteminfo.txt"
call :MKLINK "..\client\scripts\table\shipinfo.txt"
call :MKLINK "..\client\scripts\table\shipiteminfo.txt"
call :MKLINK "..\client\scripts\table\skillinfo.txt"
call :MKLINK "..\client\scripts\table\skilleff.txt"
cd ..

echo Copy dbs to db_in_use folder (to prevent conficts with any db updates)...
if not exist ".\db_in_use\" mkdir db_in_use
if %MSSQL2000%==y (
	copy /y /b db_2000\AccountServer_Data.MDF db_in_use\AccountServer_Data.MDF
	copy /y /b db_2000\AccountServer_log.LDF db_in_use\AccountServer_log.LDF
	copy /y /b db_2000\GameDB_Data.MDF db_in_use\GameDB_Data.MDF
	copy /y /b db_2000\GameDB_Log.LDF db_in_use\GameDB_Log.LDF
) else (
	copy /y /b db_2008\accountserver.mdf db_in_use\accountserver.mdf
	copy /y /b db_2008\accountserver_1.ldf db_in_use\accountserver_1.ldf
	copy /y /b db_2008\gamedb.mdf db_in_use\gamedb.mdf
	copy /y /b db_2008\gamedb_1.ldf db_in_use\gamedb_1.ldf
)

if %DEFAULT_MOD%==y (
	rem This part is only used if needing default mod
	rem Remember to adjust the path in initial.lua in resources/script folder.
	
	echo Touching files so that new ones can be created by running gameserver...
	cd client\scripts\table
	echo $null >> "areaset.txt"
	echo $null >> "characterinfo.txt"
	echo $null >> "iteminfo.txt"
	echo $null >> "shipinfo.txt"
	echo $null >> "shipiteminfo.txt"
	echo $null >> "skilleff.txt"
	echo $null >> "skillinfo.txt"
	cd ..\lua\table
	echo $null >> "stonehint_raw.lua"
	cd ..\..\..\..\resource/script/default/raw/
	echo $null >> "npcs.lua"
	echo $null >> "skills.lua"
	echo $null >> "skill_books.lua"
	cd ..\..\..\..
	
	echo Running game server if SQL Server is running
	echo Please close gameserver if it keeps running
	cd server
	GameServer.exe
	cd ..
)

echo Compiling game table files... Press OK on dialog boxes.
cd client
call compile.bat
cd ..
echo Finished.
pause
:exit
@echo on
exit /B

rem Procedure definitions

rem Makes additional checks on mklink to make sure script doesn't spit errors non-stop
rem Also prevents script execution if files are missing
:MKLINK
	if not exist %~nx1 (
		if ADMIN==n (
			goto :FILE_DOESNT_EXIST
		) else mklink %~nx1 %1
	)
	exit /B
	
:MKLINK_DIFFNAME
	if not exist %1 (
		if ADMIN==n (
			goto :FILE_DOESNT_EXIST
		) else mklink %1 %2
	)
	exit /B
	
:MKLINK_DIFFNAME_J
	if not exist %1 (
		if ADMIN==n (
			goto :FILE_DOESNT_EXIST
		) else mklink /J %1 %2
	)
	exit /B

rem https://stackoverflow.com/questions/1645843/resolve-absolute-path-from-relative-path-and-or-file-name
:NORMALIZEPATH
  set RETVAL=%~dpfn1
  exit /B

:FILE_DOESNT_EXIST
	call :NORMALIZEPATH %2
	echo "File %RETVAL% doesn't exist! Run the script as admin for the symlinking to work!"
	pause
	exit